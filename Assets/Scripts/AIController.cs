using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIController : MonoBehaviour
{
    [SerializeField] private GameObject _target;
    private UnitMovement _unitMovement;
    private Vector2 _movementVector;

    private void Start()
    {
        _unitMovement = GetComponent<UnitMovement>();
    }

    private void Update()
    {
        Vector2 targetDirection = new Vector2(_target.transform.position.x - transform.position.x, _target.transform.position.y - transform.position.y);
        _movementVector = targetDirection.normalized;
    }

    private void FixedUpdate()
    {
        _unitMovement.Move(_movementVector);
    }
}