using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestRaycast : MonoBehaviour
{
    [SerializeField] private GameObject _target;
    [SerializeField] private float _distance;
    [SerializeField] private float _duration;
    [SerializeField] private SphereCollider _sphereCollider;

    [ContextMenu("Target")]
    public void CastRaycast()
    {
        float targetDistance = Vector3.Distance(this.transform.position, _target.transform.position);

        // Use Sphere Collider radius as distance for raycast
        RaycastHit[] hits;
        hits = Physics.RaycastAll(transform.position, transform.forward, _distance);
        Debug.DrawRay(transform.position, transform.forward * _distance, Color.green, _duration);

        bool hasObstruction = false;

        // Look for the target first and cache distance
        // Then check for all object hit if its nearer then it has an obstruction
        foreach (RaycastHit hit in hits)
        {
            if (hit.transform.gameObject == _target)
                continue;



            //hit.transform.GetComponent<Renderer>().material.shader = Shader.Find("Transparent/Diffuse");
            if (hit.distance + GetComponent<CapsuleCollider>().radius < targetDistance)
            {
                hasObstruction = true; 
                break;
            }
        }

        if (hasObstruction)
            Debug.Log("Has Obstruction");
        else
            Debug.Log("Clear to Fire");
    }

    [ContextMenu("CheckDistance")]
    private void CheckDistance()
    {
        Debug.Log(Vector3.Distance(this.transform.position, _target.transform.position));
    }
    //// Update is called once per frame
    //void Update()
    //{
    //    Physics.RaycastAll(transform.position, )
    //}

    [ContextMenu("CheckSpehereColliderRadius")]
    private void CheckSphereColliderRadius()
    {
        Debug.Log(_sphereCollider.radius);
    }
}
