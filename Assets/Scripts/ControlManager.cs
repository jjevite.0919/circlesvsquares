using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using System;
using UnityEngine.InputSystem.EnhancedTouch; 
using EnchanedTouch = UnityEngine.InputSystem.EnhancedTouch;

public class ControlManager : MonoBehaviour
{
    private Finger finger1;

    private void OnEnable()
    {
        EnhancedTouchSupport.Enable();
        EnchanedTouch.Touch.onFingerDown += Touch_onFingerDown;
        EnchanedTouch.Touch.onFingerMove += Touch_onFingerMove;
        EnchanedTouch.Touch.onFingerUp += Touch_onFingerUp;
    }

    private void OnDisable()
    {
        EnchanedTouch.Touch.onFingerDown -= Touch_onFingerDown;
        EnchanedTouch.Touch.onFingerMove -= Touch_onFingerMove;
        EnchanedTouch.Touch.onFingerUp -= Touch_onFingerUp;
        EnhancedTouchSupport.Disable();
    }

    private void Touch_onFingerUp(Finger obj)
    {
        throw new NotImplementedException();
    }

    private void Touch_onFingerMove(Finger obj)
    {
        throw new NotImplementedException();
    }

    private void Touch_onFingerDown(Finger obj)
    {
        if (finger1 == null)
            finger1 = obj;
    }



}
